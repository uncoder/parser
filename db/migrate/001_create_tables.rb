class CreateTables < ActiveRecord::Migration
  def change
    create_table :stores, force: true do |t|
      t.string :title
      t.string :code
    end
    add_index :stores, :code

    create_table :quantities, force: true do |t|
      t.references :store
      t.references :good
      t.string :qty
    end
    add_index :quantities, :good_id

    create_table :categories, force: true do |t|
      t.string :title
      t.references :parent, default: nil
    end

    create_table :goods, force: true do |t|
      t.integer :foreign_id
      t.string :title
      t.string :image
      t.decimal :price, precision: 8, scale: 2
      t.references :category
    end
  end
end