class Good < ActiveRecord::Base
  validates :title, presence: true
  validates :category_id, presence: true

  has_many :quantities
end