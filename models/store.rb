class Store < ActiveRecord::Base
  validates :title, presence: true
  validates :code, presence: true, uniqueness: true
end