class Quantity < ActiveRecord::Base
  validates :good_id, presence: true
  validates :store_id, presence: true

  belongs_to :good
end