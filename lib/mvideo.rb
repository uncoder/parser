module Mvideo
  class Parser
    def initialize(options)
      @stores = {}
      @domain = "http://" + options[:domain]
      @start_url = @domain + options[:start_path]
    end

    def perform
      p 'Parsing stores...'
      get_stores
      p 'Parsing main categories...'
      categories = get_categories
      p 'Parsing subcategories...'
      subcategories = get_subcategories categories
      subcategories.each do |subcategory|
        p "Parsing goods data for #{subcategory[:title]}..."
        @goods, @qtys = [], []
        get_goods_info subcategory
        p 'Saving goods...'
        save_goods
      end
      p 'All done!'
    end

    private

    def get_stores(page = 1, pages = nil)
      url = @domain + "/shops/store-list?page=#{page}"
      doc = Nokogiri::HTML(open(url))
      doc.css('li.store-locator-list-item').each do |store|
        s = Store.create code: store["id"], title: store.css('.name h3').text.strip
        @stores[s.code] = s.id
      end
      pages_count = pages || doc.css('.pagination ul li a.pagination-item-link').last.try(:text).try(:to_i) || 1
      get_stores(page + 1, pages_count) if pages_count > page
    end

    def get_categories
      doc = Nokogiri::HTML(open(@start_url))
      categories = doc.css('a.header-nav-item-link')
        .map{|cat| {title: cat.css("span").first.text, url: cat["href"]}}
        .reject{|cat| cat[:url] == "#"}
      categories.each_with_index do |category, index|
        categories[index][:id] = Category.create(title: category[:title]).id
      end
    end

    def get_subcategories(categories)
      subcategories = []
      threads = []
      categories.each do |category|
        threads << Thread.new do
          category_url = @domain + category[:url]
          doc = Nokogiri::HTML(open(category_url)) 
          subcategories << doc.css('li.sidebar-category a')
            .map{|cat| {title: cat.text, url: cat["href"], parent: category[:id]}}
        end
      end
      threads.each { |t| t.join }
      subcategories.flatten!.each_with_index do |category, index|
        subcategories[index][:id] = Category.create(title: category[:title], parent_id: category[:parent]).id
      end
    end

    def get_goods_info(category, page = 1, pages = nil)
      p "page #{page}"
      page_url = @domain + category[:url] + "/f/page=#{page}"
      begin
        doc = Nokogiri::HTML(open(page_url))
        threads = []
        doc.css('.product-tiles-list .product-tile').each do |good|
          threads << Thread.new do
            title = good.css('.product-tile-title-link').first['title']
            foreign_id = good.css('.product-tile-title-link').first['href'][/\d+$/].to_i
            price = good.css('.product-price-current').text
            image = "http:#{good.css('img.product-tile-picture-image').first['data-original']}"
            stores_url = @domain + good.css('.product-tile-title-link').first['href'] + '?ssb_block=availabilityContentBlockContainer&ajax=true'
            @goods << {title: title, price: price, image: image, category_id: category[:id], foreign_id: foreign_id}
            begin
              s_doc = Nokogiri::HTML(open(stores_url), nil, 'utf-8')
              s_doc.css('li.store-locator-list-item').each do |store|
                s_code = store.css('.name h3 a').first["href"][/S\d+/]
                @qtys << {foreign_id: foreign_id, store_id: @stores[s_code], qty: store.css('.stock i').first["data-title"]}
              end
            rescue
              p "Error loading #{stores_url}"
            end
          end
          threads.each { |t| t.join }
        end
      pages_count = pages || doc.css('.pagination ul li a.pagination-item-link').last.try(:text).try(:to_i) || 1
      get_goods_info(category, page + 1, pages_count) if pages_count > page
      rescue
        p "Error loading #{page_url}"
      end
    end

    def save_goods
      map = Good.create(@goods).group_by &:foreign_id
      map.each {|k, v| map[k] = v.first.id}
      @qtys.each_with_index do |q, index|
        @qtys[index][:good_id] = map[q[:foreign_id]]
        @qtys[index].delete(:foreign_id)
      end
      Quantity.create @qtys
      @goods = []
      @qtys = []
    end
  end
end