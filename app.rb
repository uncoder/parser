require 'active_record'
require 'sqlite3'
require 'logger'
require 'open-uri'
require 'nokogiri'

ActiveRecord::Base.logger = Logger.new('log/active_record.log')
config = YAML::load File.read('config/database.yml')
ActiveRecord::Base.establish_connection config

Dir["./lib/*.rb"].each {|file| require file }
Dir["./models/*.rb"].each {|file| require file }